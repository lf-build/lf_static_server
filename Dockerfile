FROM node:slim

WORKDIR /app
RUN npm install

RUN npm install -g http-server 

WORKDIR /public

ENTRYPOINT http-server -d --cors=".*"